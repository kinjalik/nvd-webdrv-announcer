import { Pool as PGConnection, QueryResult } from 'pg';
import { ConfigParam } from './../interfaces/ConfigParam';

class DatabaseConnect {
  connection: PGConnection;
  connectionString: string;

  constructor(connectionString: string) {
    this.connectionString = connectionString;
  }

  async connect(): Promise<void> {
    this.connection = new PGConnection({
      connectionString: this.connectionString,
    });

    await this.connection.connect();
  }

  async getTable(tableName: string): Promise<QueryResult> {
    const queryText:string = `SELECT * FROM ${tableName}`;
    const res: QueryResult = await this.connection.query(queryText);
    return res;
  }

  async getUser(chatId:number):Promise<{
    id?:number,
    chatId?: number,
    status?: boolean
  }> {
    const queryText: string = `SELECT * FROM subscription_list WHERE chat_id=${chatId}`;
    console.log('DB query text:', queryText);
    let result: {
      id?:number,
      chatId?: number,
      status?: boolean
    } = null;

    const buffer = await this.connection.query(queryText);
    console.log('Buffer:', queryText);
    result = <{
      id?:number,
      chatId?: number,
      status?: boolean
    }> <unknown> buffer.rows;

    return result;
  }

  async addUser(chatId:number):Promise<boolean> {
    const queryText = `INSERT INTO subscription_list(chat_id, sub_status) VALUES(${chatId}, 't');`;
    console.log('DB query text:', queryText);
    try {
      await this.connection.query(queryText);
      return true;
    } catch (e) {
      return false;
    }
  }

  async updateUser(chatId:number, status:boolean):Promise<void> {
    const queryText = `UPDATE subscription_list SET sub_status='${status ? 't' : 'f'}' WHERE chat_id='${chatId}'`;
    await this.connection.query(queryText);
  }

  async getConfig(): Promise<ConfigParam[]> {
    const queryText: string = 'SELECT * FROM bot_config';
    let res: QueryResult = null;
    res = await this.connection.query(queryText);
    return <Array<ConfigParam>> res.rows;
  }

  async getConfigParam(param:string): Promise<ConfigParam> {
    const qRes: Array<ConfigParam> = await this.getConfig();
    let result: ConfigParam = null;

    qRes.forEach((pair: ConfigParam) => {
      if (pair.param === param) {
        result = pair;
      }
    });
    return result;
  }

  async setConfigParam(param: string, value: string): Promise<void> {
    await this.connection.query('UPDATE bot_config SET value=$1::text WHERE param=$2::text', [value, param]);
  }
}

export default DatabaseConnect;
