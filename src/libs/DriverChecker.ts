import plist = require('plist');
import fetch from 'node-fetch';
import Log = require('log4js');
import DatabaseConnect from './DatabaseConnect';
import { ConfigParam } from './../interfaces/ConfigParam';
import { DriverVersion } from './../interfaces/DriverVersion';

Log.configure({
  appenders: {
    out: { type: 'stdout', layout: {
      type: 'pattern',
      pattern: '%c.%p — %m',
    }}
  },
  categories: { default: { appenders: ['out'], level: 'debug' } }
});

const log = Log.getLogger('DriverChecker');

export default class DriverChecker {
  database: DatabaseConnect;
  lastVersion: ConfigParam;

  async init(dbConnect: DatabaseConnect) {
    log.info('Initialising...');
    this.database = dbConnect;
    this.lastVersion = await this.database.getConfigParam('last_sys_ver');
    log.info('Initialisated successfully.');
  }

  static async updateChannel(dbConnect: DatabaseConnect): Promise<string> {
    let res: ConfigParam = null;

    res = await dbConnect.getConfigParam('driver_update_channel');

    return res.value;
  }

  static async getLastVersion(dbConnect: DatabaseConnect) {
    log.debug(`Update channel: ${await this.updateChannel(dbConnect)}`);

    const source: string = await this.updateChannel(dbConnect);

    let resp = await fetch(source, {
      method: 'get'
    })
    let raw = <{
      updates: DriverVersion[],
      
    }> <unknown>plist.parse(await resp.text())
    const verList: DriverVersion[] = raw.updates;
    const lastVer: DriverVersion = verList[0];
    log.debug(`Version check: ${lastVer.version}`);
    return lastVer;
  }
}
