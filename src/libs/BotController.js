import Telegraf, { Markup, Extra } from 'telegraf';
const Log = require('log4js');
import DriverChecker from './DriverChecker';
import { isNull } from 'util';

Log.configure({
  appenders: {
    out: {
      type: 'stdout', 
      layout: {
        type: 'pattern',
        pattern: '%c.%p — %m',
      }
    }
  },
  categories: { default: { appenders: ['out'], level: 'debug' } }
});

const log = Log.getLogger('BOT');


export default class BotController {

  async init(dbConnect) {
    this.dbConnect = dbConnect;
    const tokenCfg = await this.dbConnect.getConfigParam('TG_TOKEN');
    this.token = tokenCfg.value;

    this.tgrf = new Telegraf(this.token);
    this.tgrm = this.tgrf.telegram;
    await this.sendMessage(259652278, `Bot v3 started at ${new Date().toString()}`);
    log.info('Bot has been started.');

    const lastVerCfg = await this.dbConnect.getConfigParam('last_sys_ver');
    this.lastVer = lastVerCfg.value;
  }

  async sendMessage(target, text) {
    try {
      await this.tgrm.sendMessage(target, text);
      log.debug(`Message -> ${target}`);
    } catch (err) {
      err.inStack = err.stack;
      Error.captureStackTrace(err);
      throw (err);
    }
  }

  async testMethod() {
  }

  async startDriverWatch() {
    this.interval = setInterval(async () => {
      log.debug('Driver check cycle..');
      try {
        const lastVerObj = await DriverChecker.getLastVersion(this.dbConnect);
        if (lastVerObj.version !== this.lastVer) {
          log.info(`Old: ${this.lastVer}. New: ${lastVerObj.version}`);
          log.info('New driver version detected.');
          this.lastVer = lastVerObj.version;
          await Promise.all([
            this.dbConnect.setConfigParam('last_sys_ver', this.lastVer),
            this.notifyNewVersion(lastVerObj),
          ]);
        }
      } catch (e) {
        log.error(`Error version checking: ${e}`);
        log.error(`${e.stack}`);
      }
    }, 30000); // 5 min * 60 sec * 1000 ms
  }

  async notifyNewVersion(version) {
    try {
      const resp = await this.dbConnect.getTable('subscription_list');
      const users = resp.rows;

      users.forEach((elem) => {
        if (elem.status) {
          this.sendMessage(elem.chat_id, `New version of driver released!
Nvidia WebDriver ${version.version}
Download Link: ${version.downloadURL}`);
        }
      });
    } catch (e) {
      log.error(`Failed to notify users: ${e}`);
    }
  }

  async startChatWatch() {
    this.tgrf.help((ctx) => {
      ctx.reply('Help message');
      log.debug(`User ${ctx.update.message.chat.id} (${ctx.update.message.chat.username}) executed /help`);
    });

    this.tgrf.command('notifications', async (ctx) => {
      let chatId = await ctx.getChat();
      chatId = chatId.id;
      const user = await this.dbConnect.getUser(chatId);
      let isTurnedOn = false;
      if (isNull(user)) {
        await this.dbConnect.addUser(chatId);
        return true;
      } 
      if (user.status) {
        await this.dbConnect.updateUser(chatId, false);
        ctx.reply('You were succesfully turned notifications OFF');
      } else {
        await this.dbConnect.updateUser(chatId, true);
        ctx.reply('You were succesfully turned notifications ON');
      }
    });

    this.tgrf.command('last_version', async (ctx) => {
      log.debug(`User ${ctx.update.message.chat.id} (${ctx.update.message.chat.username}) executed /last_version`);
      try {
        const result = await DriverChecker.getLastVersion(this.dbConnect);
        ctx.reply(`Last version of Driver: ${result.version} for Mac OS Buld ${result.OS}.
Download Link: ${result.downloadURL}`, {

          });
        if (result.models.length !== 0) {
          let msg = 'This version specified for those Mac models:';
          result.models.forEach((model) => {
            msg += `\n- ${model}`;
          });
          ctx.reply(msg);
        }
      } catch (e) {
        log.error(e);
      }
    });

    this.tgrf.startPolling();
    log.info('Polling started');
  }
}

