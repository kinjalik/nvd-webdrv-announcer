import Log = require('log4js');
import DatabaseConnect from './libs/DatabaseConnect';
import TelegramBot from './libs/BotController';
import DriverChecker from './libs/DriverChecker';

Log.configure({
  appenders: {
    out: { type: 'stdout', layout: {
      type: 'pattern',
      pattern: '%c.%p — %m',
    }}
  },
  categories: { default: { appenders: ['out'], level: 'debug' } }
});

const log: Log.Logger = Log.getLogger('CORE');

const connect: DatabaseConnect = new DatabaseConnect(process.env.DATABASE_URL);
const bot: TelegramBot = new TelegramBot();
const checker: DriverChecker = new DriverChecker();

function criticalLog(error: Error) {
  log.error(String(error));
  log.error(`Trace: ${error.stack}`);
}

async function botInit(dbConnect: DatabaseConnect) {
  try {
    await bot.init(dbConnect);
    await bot.startChatWatch();
    await bot.startDriverWatch();
  } catch (e) {
    criticalLog(e);
  }
}
async function checkerInit(dbConnect: DatabaseConnect) {
  try {
    await checker.init(dbConnect);
  } catch (e) {
    criticalLog(e);
  }
}
function init() {
  Promise.all([
    botInit(connect),
    checkerInit(connect),
  ]);
}

(async () => {
  try {
    await connect.connect();
  } catch (e) {
    criticalLog(e);
  }
  init();
})();
