export interface DriverVersion {
  version: string;
  OS: string;
  models: string[];
  seize: string;
  checksum: string;
  downloadURL: string;
}