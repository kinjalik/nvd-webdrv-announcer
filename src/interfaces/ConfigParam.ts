export interface ConfigParam {
  param: string;
  value: string;
}